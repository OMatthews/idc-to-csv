## Converter for Ibeo idc files to CSV

### Interface

Make sure all idc files are in subdirectory called "idcs"

* Call: `python read_idc.py <file name>` to extract file
* Call: `python read_idc.py all` to extract all files in idc folder

csvs will be in sub-folders inside of "csvs"