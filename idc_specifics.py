# -*- coding: utf-8 -*-
"""
Created on Fri Mar 26 13:10:11 2021

@author: Oliver.Matthews
"""
from utils import read_int, read_double, read_double_vector, read_float_vector, read_time, read_float, analyse_struct, analyse_list, get_temp_key, temp
import os
import pickle


def find_magic_word_indexes(file_name, p_name):
    file_len = os.stat(file_name).st_size

    with open(file_name, 'rb') as file:
        i = 0
        magic_word_indexes = []
        while(i <= file_len):
            file.seek(i)
            if (read_int(file.read(4)) == 0xaffec0c2):
                magic_word_indexes.append(i)
            i += 1
        pickle.dump(magic_word_indexes, open(p_name, 'wb'))
    return magic_word_indexes

def print_message_types(file_name, magic_word_indexes):
    with open(file_name, 'rb') as file:
        message_types = set()
        for magic_word_index in magic_word_indexes:
            file.seek(magic_word_index + 14)
            message_types.add(file.read(2).hex())
    print(message_types)
        
    
def get_scans(file_name, magic_word_indexes):
    scans = []
    with open(file_name, 'rb') as file:
        for magic_word_index in magic_word_indexes:
            file.seek(magic_word_index)
            header = file.read(24)
            data_type = header[14:16].hex()
            # Look for scan
            if data_type == '2340':
                message_size = read_int(header[8:12])
                ntpTime = read_time(header[16:24])
                payload = file.read(message_size)
                scans.append(analyse_struct(payload, scan_format))
                
    return scans


scanner_header = [{'name': 'deviceId', 'callback' : read_int, 'n_bytes' : 8},
                  {'name': 'scannerType', 'callback' : None, 'n_bytes' : 1},
                  {'name': 'coordinateSystem', 'callback' : read_int, 'n_bytes' : 1},
                  {'name': 'horizontalFieldOfView', 'callback' : read_double, 'n_bytes' : 8},
                  {'name': 'verticalFieldOfView', 'callback' : read_double, 'n_bytes' : 8},
                  {'name': 'mountingPosition', 'callback' : read_double_vector, 'n_bytes' : 24},
                  {'name': 'mountingOrientation', 'callback' : read_double_vector, 'n_bytes' : 24},
                  {'name': 'horizontalResolution', 'callback' : read_double, 'n_bytes' : 8},
                  {'name': 'verticalResolution', 'callback' : read_double, 'n_bytes' : 8}
                   ]

scan_point_format = [{'name': 'position', 'callback' : read_float_vector, 'n_bytes' : 12},
                  {'name': 'timestampOffset', 'callback' : read_int, 'n_bytes' : 4},
                  {'name': 'radialDistance', 'callback' : read_float, 'n_bytes' : 4},
                  {'name': 'verticalId', 'callback' : read_int, 'n_bytes' : 2},
                  {'name': 'horizontalId', 'callback' : read_int, 'n_bytes' : 2},
                  {'name': 'echoId', 'callback' : read_int, 'n_bytes' : 1},
                  {'name': 'existenceMeasure', 'callback' : read_float, 'n_bytes' : 4},
                  {'name': 'nbFollowingEchoes', 'callback' : read_int, 'n_bytes' : 1}
                   ]

scan_format = [{'name': 'scanStartTime', 'callback' : read_time, 'n_bytes' : 8},
               {'name': 'scanEndTime', 'callback' : read_time, 'n_bytes' : 8},
               {'name': 'scannerInfo', 'callback' : lambda x: analyse_struct(x, scanner_header), 'n_bytes' : 234},
               {'name': 'scanNumber', 'callback' : read_int, 'n_bytes' : 4},
               {'name': 'nbOfScanPoints', 'callback' : read_int, 'n_bytes' : 4},
               {'name': 'scanPoints', 'callback' : lambda x: analyse_list(x, 30, lambda y: analyse_struct(y, scan_point_format)), 'n_bytes' : lambda: 30 * get_temp_key('nbOfScanPoints')},
               {'name': 'nbOfInfoVectors', 'callback' : read_int, 'n_bytes' : 4},
               {'name': 'scanPointInfos', 'callback' : lambda x: analyse_list(x, lambda: 1 + 4 * get_temp_key('nbOfScanPoints'), lambda y: analyse_struct(y, scan_point_info_list_format)), 'n_bytes' : lambda: get_temp_key('nbOfInfoVectors') * (1 + 4 * get_temp_key('nbOfScanPoints'))}
               ]

scan_point_info_list_format = [{'name': 'informationType', 'callback' : read_int, 'n_bytes' : 1},
                               {'name': 'scanInfos', 'callback' : lambda x: analyse_list(x, 4, read_float), 'n_bytes' : lambda: 4 * get_temp_key('nbOfScanPoints')},
                               ]

informationType_enum = {0x00 : "Intensity",
                        0x01 : "PulseWidth",
                        0x02 : "RadialVelocity",
                        0x04: "StaticProbability",
                        0x05: "BloomingMeasure",
                        0x80: "MatchedToRef ObjectId"}