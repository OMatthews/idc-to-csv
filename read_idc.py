# -*- coding: utf-8 -*-
"""
Created on Thu Mar 25 18:23:44 2021

@author: Oliver.Matthews
"""
import os
import pickle
from idc_specifics import find_magic_word_indexes, print_message_types, get_scans, informationType_enum
from pathlib import Path
import csv
import argparse
         
def expand_list(ls):
    outdic = {}
    for key in ls[0].keys():
        if type(ls[0][key]) is tuple:
            outdic[key + '_x'] = [point[key][0] for point in ls]
            outdic[key + '_y'] = [point[key][1] for point in ls]
            outdic[key + '_z'] = [point[key][2] for point in ls]
        else:
            outdic[key] = [point[key] for point in ls]
    return outdic
    
def csv_write(scans, base_dir):
    scan_infos_dict = {}
    for scan in scans:
        scan_info_dict = {}
        for key in scan:
            if not ('scanPointInfos' == key or 'scanPoints' == key or 'scannerInfo' == key):
                scan_info_dict[key] = scan[key]
                
        for key in scan['scannerInfo']:
            if type(scan['scannerInfo'][key]) is tuple:
                scan_info_dict[key + '_x'], scan_info_dict[key + '_y'], scan_info_dict[key + '_z'] = scan['scannerInfo'][key]
            else:
                scan_info_dict[key] = scan['scannerInfo'][key]            
            
        if scan_infos_dict == {}:
            scan_infos_dict = {key : [value] for key, value in scan_info_dict.items()}
        else:
            for key, value in scan_info_dict.items():
                scan_infos_dict[key].append(value)
            
        out = expand_list(scan['scanPoints'])
            
        for dic in scan['scanPointInfos']:
            out[informationType_enum[dic['informationType']]] = dic['scanInfos']
    
        with open(base_dir / ('Scan' + str(scan['scanNumber']) + '.csv'), 'w', newline='') as fd:
            writer = csv.writer(fd) 
            writer.writerow(out.keys())
            writer.writerows(zip(*out.values()))

    with open(base_dir / ('ScanInfo.csv'), 'w', newline='') as fd:
        writer = csv.writer(fd) 
        writer.writerow(scan_infos_dict.keys())
        writer.writerows(zip(*scan_infos_dict.values()))
        
    
            
    
def generate_csvs(file, base_save_dir, p_dir):
    p_file = p_dir / (file.stem + '.p')
    save_dir = base_save_dir / file.stem
    if not os.path.exists(save_dir):
        os.mkdir(save_dir)
        
    if os.path.exists(p_file):
        magic_word_indexes = pickle.load(open(p_file, 'rb'))        
    else:
        magic_word_indexes = find_magic_word_indexes(file, p_file)
        
        
    scans = get_scans(file, magic_word_indexes)
    csv_write(scans, save_dir)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('file',
                        metavar='file',
                        type=str,
                        help='the file name to parse')
    args = parser.parse_args()
    file_name = args.file

    base_dir = Path(os.getcwd()) 
    save_dir = base_dir / 'csvs'
    p_dir = base_dir / 'p_files'
    if not os.path.exists(save_dir):
        os.mkdir(save_dir)
    if not os.path.exists(p_dir):
        os.mkdir(p_dir)

    idc_dir = base_dir / 'idcs'

    if file_name == 'all':
        for file_name in os.listdir(idc_dir):
            if file_name.endswith('.idc'):
                print('Extracting file: ' + file_name)
                generate_csvs(idc_dir / file_name, save_dir, p_dir)
    else:
        generate_csvs(idc_dir / file_name, save_dir, p_dir)
