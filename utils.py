# -*- coding: utf-8 -*-
"""
Created on Fri Mar 26 09:38:13 2021

@author: Oliver.Matthews
"""
import struct

temp = {}

def read_int(b):
    return int.from_bytes(b, 'big')

def read_double(bs):
    return struct.unpack('>d', bs)[0]

def read_float(bs):
    return struct.unpack('>f', bs)[0]

def read_double_vector(bs):
    n_doubles = int(len(bs) / 8)
    return struct.unpack('>' + 'd' * n_doubles, bs)

def read_float_vector(bs):
    n_doubles = int(len(bs) / 4)
    return struct.unpack('>' + 'f' * n_doubles, bs)

def read_time(bs):
    return read_int(bs[:4]) + (1 / read_int(bs[4:]))

def analyse_struct(payload, info):
    global temp
    out_dict = {}
    offset = 0
    for entry in info:
        if not type(entry['n_bytes']) is int:
            n_bytes = entry['n_bytes']()
        else:
            n_bytes = entry['n_bytes']
            
        next_offset = offset + n_bytes
        if not entry['callback'] is None:
            out_dict[entry['name']] = entry['callback'](payload[offset: next_offset])
            temp[entry['name']] = out_dict[entry['name']]
        offset = next_offset
    return out_dict

def analyse_list(payload, n_bytes, callback):
    if not type(n_bytes) is int:
        n_bytes = n_bytes()
    out_list = []
    offset = 0 
    while(offset < len(payload)):
        next_offset = offset + n_bytes
        out_list.append(callback(payload[offset: next_offset]))
        offset = next_offset
    return out_list

def get_temp_key(key):
    global temp
    return temp[key]
